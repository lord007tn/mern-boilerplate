import React, { Fragment, useEffect } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
//Redux
import {Provider} from 'react-redux'
import store from './store'
//checkAuthMiddleware
import setAuthToken from './utils/setAuthToken'
import {loadUser} from './actions/auth.actions'
//components
import Login from './components/auth/Login/Login'
import Register from './components/auth/Register/Register'

if (localStorage.token) {
  setAuthToken(localStorage.token)
}
const App = () => {
    useEffect(() => {
      store.dispatch(loadUser())
    }, [])
  return (
    <Provider store= {store}>
    <Fragment>
    <Router>
      <Switch>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/register" component={Register}/>
      </Switch>
    </Router>
    </Fragment>
    </Provider>

  );
}

export default App;
