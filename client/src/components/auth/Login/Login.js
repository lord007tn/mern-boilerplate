import React, {useState, Fragment} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Redirect, Link} from 'react-router-dom'
import {login} from '../../../actions/auth.actions'
import Spinner from '../../utils/Spinner/Spinner'
const Login = ({auth, login}) => {
    const [FormData, setFormData] = useState({
        email:'',
        password:'',
    });
    const {email, password} = FormData;
    const onChange = e => setFormData({...FormData, [e.target.name]: e.target.value})
    const onSubmit = e => {
      e.preventDefault()
      login(email, password);
      setFormData({email:"", password:""})
  }
    if(auth.isAuthenticated){
        return <Redirect to='/home' />
    }
    return (auth.loading ? <Spinner/> :
        <Fragment>
            <div className="min-h-screen flex items-center justify-center bg-gray-500 py-12 px-4 sm:px-6 lg:px-8">
  <div className="max-w-md w-full">
    <div>
      <h2 className="mt-6 text-center text-3xl leading-9 font-extrabold text-gray-900">
        Sign in to your account
      </h2>
    </div>
    <div className=" bg-white rounded shadow p-6 m-4">
    <form className="mt-8" onSubmit={e => onSubmit(e)}>
      <div className="rounded-md shadow-sm">
        <div>
          <input
          id="email"
          name="email"
          type="email"
          placeholder="Email"
          value={email}
          onChange={e => onChange(e)}
          required
          className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"/>
        </div>
        <div className="-mt-px">
          <input
          id="password"
          name="password"
          type="password"
          placeholder="Password"
          value= {password}
          onChange={e => onChange(e)}
          required className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"/>
        </div>
      </div>

      <div className="mt-6 flex items-center justify-between">
        <div className="flex items-center">
          <input id="remember_me" type="checkbox" className="form-checkbox h-4 w-4 text-indigo-600 transition duration-150 ease-in-out"/>
          <label htmlFor="remember_me" className="ml-2 block text-sm leading-5 text-gray-900">
            Remember me
          </label>
        </div>

        <div className="text-sm leading-5">
          <Link to="#" className="font-medium text-indigo-600 hover:text-indigo-500 focus:outline-none focus:underline transition ease-in-out duration-150">
            Forgot your password?
          </Link>
        </div>
      </div>

      <div className="mt-6">
        <button type="submit" className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
          Sign in
        </button>
      </div>
    </form>
    </div>

  </div>
</div>
        </Fragment>
    )
}

Login.propTypes = {
    auth: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    auth: state.auth
})
export default connect(mapStateToProps, {login})(Login)
