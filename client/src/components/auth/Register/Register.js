import React, {Fragment, useState} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Redirect, Link} from 'react-router-dom'
import {register} from '../../../actions/auth.actions'
import Spinner from '../../utils/Spinner/Spinner'
const Register = ({auth, register}) => {
    const [FormData, setFormData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        phoneNumber:"",
        password: "",
        password2:""
    })
    const {firstName, lastName, email, phoneNumber, password, password2} = FormData
    const onChange = e =>{
        setFormData({...FormData, [e.target.name]: e.target.value})
    }
    const onSubmit = e =>{
        e.preventDefault()
        if(password !== password2){
            setFormData({...FormData, password:"", password2:""})
        }else{
        register({firstName, lastName, email, phoneNumber, password});
        setFormData({firstName:"", lastName: "", email:"", phoneNumber:"", password:"", password2:""})
        }
     }
     if(auth.isAuthenticated){
         return <Redirect to="/"/>
     }
    return (auth.loading ? <Spinner/> :
<Fragment>
            <div className="min-h-screen flex items-center justify-center bg-gray-500 py-12 px-4 sm:px-6 lg:px-8">
  <div className="max-w-md w-full">
    <div>
      <h2 className="mt-6 text-center text-3xl leading-9 font-extrabold text-gray-900">
        Create an account
      </h2>
    </div>
    <div className=" bg-white rounded shadow p-6 m-4">
    <form className="mt-8" onSubmit={e => onSubmit(e)}>
      <div className="rounded-md shadow-sm">
        <div className="flex flex-wrap -mx-3 mb-6">
        <div className="md:w-1/2 w-full px-3 mb-6 md:mb-0">
          <input
          id="firstName"
          name="firstName"
          type="text"
          placeholder="First Name"
          value={firstName}
          onChange={e => onChange(e)}
          required
          className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"/>
        </div>
        <div className="md:w-1/2 w-full">
          <input
          id="lastName"
          name="lastName"
          type="text"
          placeholder="Last Name"
          value={lastName}
          onChange={e => onChange(e)}
          required
          className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"/>
        </div>
        </div>
        <div className="mb-6">
          <input
          id="email"
          name="email"
          type="email"
          placeholder="Email"
          value={email}
          onChange={e => onChange(e)}
          required
          className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"/>
        </div>
        <div className="flex flex-wrap -mx-3 mb-6">
        <div className="md:w-1/2 w-full px-3 mb-6 md:mb-0">
          <input
          id="password"
          name="password"
          type="password"
          placeholder="Password"
          value= {password}
          onChange={e => onChange(e)}
          required className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"/>
        </div>
        <div className="md:w-1/2 w-full">
          <input
          id="password2"
          name="password2"
          type="password"
          placeholder="Confirm Password"
          value= {password2}
          onChange={e => onChange(e)}
          required className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"/>
        </div>
        </div>
        <div className="md:w-1/2 w-full">
          <input
          id="phoneNumber"
          name="phoneNumber"
          type="text"
          placeholder="Phone Number"
          value= {phoneNumber}
          onChange={e => onChange(e)}
          required className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"/>
        </div>
      </div>
      <div className="mt-6">
        <button type="submit" className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
          Sign up
        </button>
      </div>
    </form>
    </div>

  </div>
</div>
        </Fragment>
    )
}

Register.propTypes = {
    auth: PropTypes.object.isRequired,
    register: PropTypes.func.isRequired,
}
const mapStateToProps = state => ({
    auth: state.auth
})
export default connect(mapStateToProps, {register})(Register)
